# Maintainer: Levente Polyak <anthraxx[at]archlinux[dot]org>

pkgname=gef
pkgver=2024.01
pkgrel=5
pkgdesc='Multi-Architecture GDB Enhanced Features for Exploiters & Reverse-Engineers'
url='https://github.com/hugsy/gef'
arch=('any')
license=('MIT')
depends=('gdb' 'python')
optdepends=(
  'python-capstone: extended disassemble support'
  'python-keystone: assembler support'
  'python-unicorn: emulation support'
  'ropgadget: ROP gadget support'
)
source=(https://github.com/hugsy/gef/archive/${pkgver}/${pkgname}-${pkgver}.tar.gz.tar.gz)
sha512sums=('d8dbc308f864e434ea79eaf44e329e11f2cea9836a5dcdc019bd621bbec6792f70e58edd9fd8b3c434dc61a17fbac08394e281530423081beb1cc1192aece79c')
b2sums=('f5409e592c50dbbe65761e7727f28aa7a96b130449f187e392cc0170707b7cfb2d1bf9e75581dce7dce8ff6d004ffc624d34a46b7ba35ab434a5671662860bdb')

build() {
  cd ${pkgname}-${pkgver}
  python -m compileall .
  python -O -m compileall .
}

package() {
  cd ${pkgname}-${pkgver}
  install -Dm 644 *.py -t "${pkgdir}/usr/share/${pkgname}"
  install -Dm 644 __pycache__/* -t "${pkgdir}/usr/share/${pkgname}/__pycache__"
  install -Dm 644 README.md -t "${pkgdir}/usr/share/doc/${pkgname}"
  install -Dm 644 LICENSE -t "${pkgdir}/usr/share/licenses/${pkgname}"
  install -d "${pkgdir}/usr/share/doc/${pkgname}"
  cp -r docs/* "${pkgdir}/usr/share/doc/${pkgname}"

}

# vim: ts=2 sw=2 et:
